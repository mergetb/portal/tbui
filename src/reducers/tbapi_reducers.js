import {
  types, prefixes,
} from 'actions/tbapi_actions';

// This is the state.
//
// {
//      users: {  // canonical user data
//          username: {
//                // whatever the REST API gives us.
//                email: "maisie@foobar.net",
//                id: 3,
//                name: "Maisie Furball",
//                username: "maisie"
//          },
//          username: {
//              ...
//          },
//          ...
//      ],
//      projects: {  // canonical project data
//          projectname: {
//              // whatever the REST API gives us
//          },
//          projectname: {
//              ...
//          },
//          ...
//      },
//      experiments: { // canonical experiment data.
//          projectname: {
//              experimentname: {
//                  // whateven REST API gives us.
//                  // Note that this includes a project name which we can
//                  // use as a key into the projects {} dictionary.
//              },
//              experimentname: {
//                  ...
//              },
//              ...
//          },
//          ...
//      },
//      members: {  // canonical members data.
//          projectname: {
//              membername: {
//                  // REST API data. Note that this includes "project" and "member" which
//                  // map directly to projectname and username for indexing into the
//                  // projects and users tables/dictionaries.
//              },
//              membername: {
//                  ...
//              },
//              ...
//          },
//          ...
//      },
//      realizations: {
//          pid: {
//              eid: [array, of, reals, names]
//              ...
//          }
//      },
//      realization: { // NOTE that pideid is the pid end eid joined via [pid, eid].join().
//          pideid: {  // This is not great but is at least readable and still unique across reals.
//              rid: {
//                  JSON from REST
//              },
//              ...
//          },
//      },
//      fetching: {     // if they exist here, they are being fetched.
//          thing: null,            // never fetched
//          anotherThing: true      // is currently being fetched
//          foobar: <time>          // was fetched at time <time>
//      },
//      modelNodesById: {
//          id: // node JSON
//          ...
//      },
//      health: true|false|null,
//      errors: {
//          ...    // GTL this is going to be redesigned.
//      },
// }
const fetchingReducerByKey = (state = null, action) => {
  // key is an Array of strings that must uniquely ID the thing being fetched.
  const { type, key, value } = action;
  switch (type) {
    case types.FETCHING_BY_KEY: {
      if (value !== false) {
        return { ...state, [key]: value };
      }
      const newstate = { ...state };
      delete newstate[key];
      return newstate;
    }

    default:
      return state;
  }
};

// This reducer expects an Array of like JSON objects and that one of
// the elements of the JSON objects will be a unique entry that IDs that
// object. When an CREATE action is given, the array will be transformed
// into a dictionary indexed by that JSON's entry.
//
// index can be a single string or an array of strings. If an array of strings,
// then the nesting of the JSON data will be as deep as the length of the array
// and each string in the array is the next key in the dictionary. Thus we support
// arbitrarily deep structures.
// ex: index = ["foo", "bar"] the resulting state is:
// foo: {
//      bar: {
//          json data
//      }
// }
//
// CREATE action rewrites all data:
//      [ {..., username=murphy, ...}, {..., username=olive,...}]
//  becomes:
//      { olive: {...}, murphy: {...} }
//
// UPDATE will add/update a single JSON entry.
// DELETE will remove a single entry.
// CLEAR will clear all state.
//
const receiveIndexedArrayReducerFor = (prefix, theKey) => {
  const receiveIndexedArrayReducer = (state = null, action) => {
    // convert Array of JSON to dictionary indexed by "theKey" from each json object.
    const toIndexedJson = (json, key) => {
      const tmp = {};
      const keys = Object.keys(json);
      for (let i = 0; i < keys.length; i += 1) {
        tmp[json[keys[i]][key]] = json[keys[i]];
      }
      return tmp;
    };
    const { type, json, index } = action;
    switch (type) {
      case prefix + types.CREATE_INDEXED_ENTRIES:
        return toIndexedJson(json, theKey);
      case prefix + types.UPDATE_INDEXED_ENTRY:
        if (state) {
          return { ...state, [index]: json };
        }
        return { [index]: json };
      case prefix + types.DELETE_INDEXED_ENTRY: {
        // We use destructured assignment to break apart the state into [index] and ...rest,
        // then just return the ...rest.
        const { [index]: _, ...newState } = state;
        return newState;
      }
      case prefix + types.CLEAR_INDEXED_ENTRIES:
        return null;
      default:
        return state;
    }
  };
  return receiveIndexedArrayReducer;
};

const receiveNamedIndexReducerFor = (prefix) => {
  const receiveNamedIndexReducer = (state = null, action) => {
    const {
      type, name, index, json,
    } = action;
    switch (type) {
      case prefix + types.UPDATE_NAMED_INDEXED_ENTRY:
        if (state && state[name]) {
          // appending to existing array
          if (Array.isArray(state[name][index]) && !Array.isArray(json)) {
            return {
              ...state,
              [name]: { ...state[name], [index]: state[name][index].concat(json) },
            };
          }
          return { ...state, [name]: { ...state[name], [index]: json } };
        } if (state && !state[name]) {
          if (index) {
            return { ...state, [name]: { [index]: json } };
          }
          return { ...state, [name]: json };
        }
        if (index) {
          return { [name]: { [index]: json } };
        }

        return { [name]: json };


      case prefix + types.DELETE_NAMED_INDEXED_ENTRY:
        if (state && state[name] && state[name][index]) {
          // confusing? yes. We use destructured assignment to break apart the state.
          // all_name = all things under the name (e.g. all experiments under a name)
          const { [name]: allIndexed, ...restState } = state;
          // restIndex = all the experiments in allIndexed except the indexed one.
          const { [index]: _, ...restIndex } = allIndexed;
          // now "rebuild" the state with everything left over.
          return { [name]: restIndex, ...restState };
        }
        return state;
      default:
        return state;
    }
  };
  return receiveNamedIndexReducer;
};

//
// reducer that just writes the given payload to the state.
//
const setPayloadValueFor = (prefix) => {
  const setPayloadValue = (state = null, action) => {
    const { type, payload } = action;
    switch (type) {
      case prefix + types.RECEIVE:
        return payload;
      default:
        return state;
    }
  };
  return setPayloadValue;
};

const setNetworkError = (state = false, action) => {
  if (action.type === 'NETWORK_ERROR') {
    return true;
  }
  return state;
};

const userNotActive = (state = false, action) => {
  if (action.type === 'USER_NOT_ACTIVE') {
    return true;
  }
  return state;
};

const reduceKeyedObjectsFor = (prefix) => {
  const reduceKeyedObjects = (state = null, action) => {
    const {
      type, payload, key, id,
    } = action;
    switch (type) {
      // we convert the array into a dict, keyed by the object's key.
      // this is more efficent for finding objects by id when needed.
      // (No need to iterate over the array.)
      case prefix + types.RECEIVE_ARRAY_OF_KEYED_OBJECTS: {
        // iterate over the array, and create keyed object of objects.
        const tmp = {};
        for (let i = 0; i < payload.length; i += 1) {
          // Use Computed object property names and destructuring to get the key/data.
          const { [key]: k } = payload[i];
          tmp[k] = payload[i];
        }
        return tmp; // replace the entire thing.
      }
      case prefix + types.RECEIVE_OBJECT_BY_ID: {
        return { ...state, [id]: payload }; // update just the keyed entry.
      }
      case prefix + types.DELETE_OBJECT_BY_ID: {
        // Use destructured assignment to break apart the state and only return "the rest"
        // minus the entry keyed by id.
        const { [id]: allIndexed, ...rest } = state;
        return rest;
      }
      default: {
        return state;
      }
    }
  };
  return reduceKeyedObjects;
};

export default {
  mtzTransition: setPayloadValueFor('mtzTransition'),
  users: reduceKeyedObjectsFor(prefixes.USERS),
  projects: reduceKeyedObjectsFor(prefixes.PROJECTS),
  sites: reduceKeyedObjectsFor('sites', 'name'),

  experiments: receiveNamedIndexReducerFor('experiments'),
  expversion: receiveNamedIndexReducerFor('expversion'),
  expversions: receiveNamedIndexReducerFor('expversions'),
  expxdcs: receiveNamedIndexReducerFor('expxdcs'),
  xdctoken: receiveNamedIndexReducerFor('xdctoken'),
  realization: receiveNamedIndexReducerFor('realization'),
  realizations: receiveNamedIndexReducerFor('realizations'),
  materialization: receiveNamedIndexReducerFor('materialization'),
  materializationStatus: receiveNamedIndexReducerFor('materializationStatus'),
  members: receiveNamedIndexReducerFor('members'),
  modelNodesById: receiveIndexedArrayReducerFor('modelNodesById', 'id'),
  modelEndpointsById: receiveIndexedArrayReducerFor('modelEndpointsById', 'id'),
  modelLinksById: receiveIndexedArrayReducerFor('modelLinksById', 'id'),
  pubkeys: receiveNamedIndexReducerFor('pubkeys'),
  health: setPayloadValueFor('health'),
  fetching: fetchingReducerByKey,
  networkError: setNetworkError,
  userNotActive,
};
