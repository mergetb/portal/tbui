import { Graph } from '@dagrejs/graphlib';

// xir node example
//    {
//      "id": "spine0",
//      "endpoints": [
//        {
//          "id": "4c5b4b81-1c8c-496a-87b1-58b3164fa35a",
//          "props": {
//            "ip": {
//              "addrs": [],
//              "mtu": 1500
//            }
//          }
//        },
//        ...
//      ],
//      "props": {
//        "color": "#575ced",
//        "kind": {
//          "value__": "moa-mprouter",
//          "constraint__": "="
//        },
//        "router": {
//          "routes": []
//        }
//      }
//    },
//
// xir link example
//    {
//      "id": "04e772c1-9672-4114-8079-15adbe90d268",
//      "endpoints": [
//        {
//          "id": "c70f5a1a-df66-491d-9546-ea345d6416d8",
//          "props": {
//            "ip": {
//              "addrs": [],
//              "mtu": 1500
//            }
//          }
//        },
//        {
//          "id": "5dcb87e7-7e44-4fc9-810e-e42d61da5f64",
//          "props": {
//            "ip": {
//              "addrs": [],
//              "mtu": 1500
//            }
//          }
//        }
//      ],
//      "props": {}
//    },

// given an endpoint, find the node that has this endpoint.
const getNodeByEndpoint = (ep, nodes) => {
  for (let ni = 0; ni < nodes.length; ni += 1) {
    if (ep.node === nodes[ni].id) {
      return nodes[ni];
    }
  }
  return null;
};

// xir2graph takes a xir as JSON and returns a graph.
// The graph has nodes and edges. The nodes are labeled
// with teh xir node id, and the content of the node is the xir props.
export const xir2graph = (xir) => {
  const g = new Graph({ directed: false, compound: false, multigraph: true });
  const { nodes, links } = xir;

  for (let nid = 0; nid < nodes.length; nid += 1) {
    // create a node indexed by id and deep copy the props.
    let p = {};
    if (Object.hasOwnProperty.call(nodes[nid], 'props')) {
      p = JSON.parse(JSON.stringify(nodes[nid].props));
    }
    g.setNode(nodes[nid].id, {
      props: p,
      type: 'node',
    });
  }

  if (!links) {
    return g;
  }

  let lanid = 0;
  for (let i = 0; i < links.length; i += 1) {
    const eplen = links[i].endpoints.length;
    if (eplen > 2) { // make a "LAN" node
      const lanname = `lan${lanid}`;
      // This will break if someone names a node "lanX"!
      g.setNode(lanname, {
        props: { type: 'LAN' },
      });

      // find all other endpoints of this LAN
      for (let ei = 0; ei < links[i].endpoints.length; ei += 1) {
        const node = getNodeByEndpoint(links[i].endpoints[ei], nodes);
        if (node !== null) {
          let p = {};
          if (Object.hasOwnProperty.call(links[i], 'props')) {
            p = JSON.parse(JSON.stringify(links[i].props));
          }
          g.setEdge(lanname, node.id, {
            props: p,
          });
        }
      }

      lanid += 1;
    } else if (eplen === 2) { // not a "LAN" so we can just link the two existing nodes.
      const a = getNodeByEndpoint(links[i].endpoints[0], nodes);
      const b = getNodeByEndpoint(links[i].endpoints[1], nodes);

      if (a !== null && b !== null) {
        let p = {};
        if (Object.hasOwnProperty.call(links[i], 'props')) {
          p = JSON.parse(JSON.stringify(links[i].props));
        }
        g.setEdge(a.id, b.id, {
          props: p,
        });
      }
    }
  }

  return g;
};

// Take a graph and return an array of nodes and links suitable
// for displaying in an d3 force directed graph. The format of the
// data structures is:
// nodes {
//    name: friendly name
//    props: { props given here will always exist in addition to whatever is in graph node props.
//       color: some color
//       group: some int, default
//       shape: some shape ("circle", "rect")
//    }
// }
// links {
//    source: index into node array of node a
//    target: index into the node array of node b
//    name: a friendly name for the link
//    props: { props given here will always exist in addition to whatever is in graph node props.
//        color: some color
//    }
// }
// d3 supported shapes:
//  circle, cross, diamond, square, star, triangle, and wye.
export const graph2d3force = (g) => {
  const res = { nodes: [], links: [] };

  const labels = g.nodes();
  for (let i = 0; i < labels.length; i += 1) {
    let props = {};
    if (Object.hasOwnProperty.call(g.node(labels[i]), 'props')) {
      props = JSON.parse(JSON.stringify(g.node(labels[i]).props));

      // force certain props to always exist.
      if (!Object.prototype.hasOwnProperty.call(props, 'group')) {
        props.group = 0;
      }
      if (!Object.prototype.hasOwnProperty.call(props, 'shape')) {
        props.shape = 'circle';
      }
      if (props.type === 'node') {
        if (!Object.prototype.hasOwnProperty.call(props, 'shape')) {
          props.shape = 'circle';
        }
      } else if (props.type === 'LAN') {
        props.shape = 'square';
        props.color = 'lightgrey';
      }
    }

    // add the node.
    res.nodes.push({
      name: labels[i],
      props,
    });
  }
  // edges example
  // 0: {v: "a", w: "lan0"}
  // 1: {v: "b", w: "lan0"}
  // 2: {v: "c", w: "lan0"}
  const edges = g.edges();
  for (let i = 0; i < edges.length; i += 1) {
    const edge = g.edge(edges[i]);
    let props = {};
    if (Object.hasOwnProperty.call(edge, 'props')) {
      props = JSON.parse(JSON.stringify(edge.props));
    }
    if (!Object.prototype.hasOwnProperty.call(props, 'color')) {
      props.color = '#808080';
    }
    res.links.push({
      source: labels.indexOf(edges[i].v),
      target: labels.indexOf(edges[i].w),
      name: `${edges[i].v}-${edges[i].w}`, // should look in props for this?
      props,
    });
  }

  return res;
};

export default {
  xir2graph,
  graph2d3force,
};
