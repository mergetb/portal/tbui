import { createStore, combineReducers, applyMiddleware } from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';

// GTL TODO only user loging in non-production builds.
// import { createLogger } from 'redux-logger';

import authReducer from 'reducers/auth_reducers';
import tbapiReducer from 'reducers/tbapi_reducers';
import notificationReducer from 'reducers/notification_reducers';

const getInitialState = () => {
  const initialState = {
  };

  return initialState;
};

export const makeReducers = (history) => combineReducers({
  router: connectRouter(history),
  ...authReducer,
  ...tbapiReducer,
  ...notificationReducer,
});


const configureStore = (opts) => {
  const store = createStore(
    makeReducers(opts.history),
    getInitialState(),
    applyMiddleware(
      routerMiddleware(opts.history),
      thunk,
      // createLogger(),
    ),
  );

  return store;
};

export default configureStore;
