import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import Loading from 'components/Dialog/Loading.jsx';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class ExperimentVersionRow extends Component {
  constructor(props) {
    super(props);
    this.loadExpVersion = this.loadExpVersion.bind(this);
  }

  componentDidMount() {
    this.loadExpVersion();
  }

  componentDidUpdate() {
    this.loadExpVersion();
  }

  loadExpVersion() {
    const {
      dispatch, pid, eid, hash,
    } = this.props;
    dispatch(TbapiActions.fetchExperimentVersion(pid, eid, hash));
  }

  render() {
    // the context of this component is a row in a table. The parent understands the component
    // enough to give proper headers in the correct order. This component is bascially here
    // to load the experiment verison on demand once the array of hashes is loaded in the parent.
    const {
      key, expver, hash, pid, eid,
    } = this.props;
    const smHash = hash.slice(0, 8);
    if (!expver) {
      return (
        <tr key={key}>
          <td>
            <Loading />
          </td>
          <td>
            <Loading />
          </td>
          <td>
            <Loading />
          </td>
          <td>
            <Loading />
          </td>
          <td>
            <Loading />
          </td>
          <td>
            <Loading />
          </td>
          <td>
            <Loading />
          </td>
          <td>
            {smHash}
          </td>
        </tr>
      );
    }
    const modelName = `${pid}.${eid}.${smHash}`;
    return (
      <tr key={key}>
        <td key={`${key}.1`}>
          {expver.pushDate}
        </td>
        <td key={`${key}.2`}>
          {expver.xir.id}
        </td>
        <td key={`${key}.3`}>
          {expver.xir.nodes ? expver.xir.nodes.length : 0}
        </td>
        <td key={`${key}.4`}>
          {expver.xir.links ? expver.xir.links.length : 0}
        </td>
        <td key={`${key}.5`}>
          {expver.who}
        </td>
        <td key={`${key}.6`}>
          {expver.src && (
            <NavLink to={{
              pathname: `/models/edit/${modelName}`,
              state: {
                model: {
                  name: modelName,
                  filename: '',
                  contents: expver.src,
                },
                keyPrefix: 'mde_',
              },
            }}
            >
              edit
            </NavLink>
          )}
        </td>
        <td key={`${key}.7`}>
          <NavLink to={{
            pathname: `/projects/${pid}/experiments/${eid}/src/${hash}`,
            state: {
              pid,
              eid,
              xhash: hash,
            },
          }}
          >
            {smHash}
          </NavLink>
        </td>
        <td className="tc-actions">
          <AddDeleteItemButton
            add
            item="Realization"
            onClick={() => this.props.onNewRlz(hash)}
            // to={`/create/realization/${pid}/${eid}/${hash}`}
          />
        </td>
      </tr>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { pid, eid, hash } = props;
  return {
    expver: selectors.getExperimentVersion(state, pid, eid, hash),
  };
};

export default connect(mapStateToProps)(ExperimentVersionRow);
