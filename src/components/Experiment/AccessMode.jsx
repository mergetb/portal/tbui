import React, { Component } from 'react';
import { FormGroup, Form } from 'react-bootstrap';

class AccessMode extends Component {
  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
    let m = props.accessMode;
    if (m) {
      // We speak upper case here. API/parent speaks lowercase.
      m = m.charAt(0).toUpperCase() + m.slice(1);
    } else {
      m = 'Public';
    }
    this.state = { accessMode: m };
  }

  handleSelect(event) {
    this.setState({ accessMode: event.target.value });
    this.props.handleSelect(event.target.value.toLowerCase());
  }

  render() {
    return (
      <FormGroup>
        <Form.Label>Access Mode</Form.Label>
        <Form.Control
          as="select"
          title={this.state.accessMode}
          disabled={this.props.disabled}
          onChange={this.handleSelect}
          value={this.state.accessMode}
        >
          <option value="Public">Public</option>
          <option value="Protected">Protected</option>
          <option value="Private">Private</option>
        </Form.Control>
      </FormGroup>
    );
  }
}

export default AccessMode;
