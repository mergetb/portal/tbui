import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import {
  Button, Card, Row, Col, Form, FormControl,
} from 'react-bootstrap';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';
import Loading from 'components/Dialog/Loading';

class ExperimentProfile extends Component {
  constructor(props) {
    super(props);
    this.getExperiment = this.getExperiment.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleDescChange = this.handleDescChange.bind(this);

    const name = this.props.experiment ? this.props.experiment.name : '';
    const desc = this.props.experiment ? this.props.experiment.description : '';
    this.state = {
      buttonTitle: this.props.create ? 'Create' : 'Update',
      buttonDisabled: !this.props.create,
      buttonAction: this.props.create ? 'create' : 'update',
      initialName: this.props.match.params.experimentname,
      expName: name,
      expDesc: desc,
      redirect: null,
    };
  }

  componentDidMount() {
    this.getExperiment();
  }

  componentDidUpdate() {
    this.getExperiment();
  }

  getExperiment() {
    const { authd, dispatch } = this.props;
    const { projectname, experimentname } = this.props.match.params;

    if (authd) {
      dispatch(TbapiActions.fetchExperiment(projectname, experimentname));
    }
  }

  getNameValidationState() {
    // for now, just no spaces.
    if (this.state.expName && this.state.expName.includes(' ')) {
      return 'error';
    }
    return 'success';
  }

  handleDescChange(event) {
    this.setState({
      buttonDisabled: false,
      expDesc: event.target.value,
    });
  }

  handleNameChange(event) {
    if (event.target.charCode === 13) {
      this.handleClick();
      return;
    }

    let newText = 'Create';
    let action = 'create';
    if (event.target.value === this.state.initialName) {
      newText = 'Update';
      action = 'update';
    }
    const disabled = this.getNameValidationState() === 'error';
    this.setState({
      buttonTitle: newText,
      buttonDisabled: disabled,
      expName: event.target.value,
      buttonAction: action,
    });
  }

  handleClick() {
    const { dispatch } = this.props;
    const { projectname } = this.props.match.params;

    if (this.state.buttonAction === 'update') {
      dispatch(TbapiActions.updateExperiment(
        projectname,
        this.state.expName,
        this.state.expDesc,
      ));
    } else {
      dispatch(TbapiActions.createExperiment(
        projectname,
        this.state.expName,
        this.state.expDesc,
      )).then(() => {
        this.setState(
          (prevState) => ({ redirect: `/projects/${projectname}/experiments/${prevState.expName}` }),
        );
      });
    }
    this.setState({
      buttonDisabled: true,
    });
  }

  render() {
    const { fetching, authd, create } = this.props;
    const { experimentname, projectname } = this.props.match.params;
    let { experiment } = this.props; // not constas user may update it.

    if (!authd) {
      return <Redirect to="/landing" />;
    }
    if (this.state.redirect) { return <Redirect to={this.state.redirect} />; }

    if (!create) {
      if (!experiment || fetching) {
        return (
          <Row>
            <Card key={0} title={`Loading experiment ${experimentname}...`} content={<Loading />} />
          </Row>
        );
      }
    } else {
      experiment = {
        project: projectname,
        name: '',
        creator: 'You!',
      };
    }

    return (
      <Card>
        <Card.Header>Experiment Profile</Card.Header>
        <Card.Body>
          <Form>
            <Form.Row>
              <Form.Group as={Col} md="4">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  bsPrefix="form-control"
                  defaultValue={experiment.name}
                  onChange={this.handleNameChange}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') this.handleClick();
                  }}
                />
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Label>Project</Form.Label>
                <Form.Control
                  type="text"
                  bsPrefix="form-control"
                  placeholder="Project"
                  readOnly
                  value={experiment.project}
                />
              </Form.Group>
              <Form.Group as={Col} md={4}>
                <Form.Label>Creator</Form.Label>
                <Form.Control
                  type="text"
                  bsPrefix="form-control"
                  readOnly
                  value={experiment.creator}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col} md="12" controlId="formControlsTextarea">
                <Form.Label>Experiment Description</Form.Label>
                <FormControl
                  rows="5"
                  as="textarea"
                  bsPrefix="form-control"
                  placeholder="Write the experiment description here."
                  readOnly={false}
                  value={this.state.expDesc}
                  onChange={this.handleDescChange}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col}>
                <Button
                  variant="outline-info"
                  className="float-right"
                  disabled={this.state.buttonDisabled}
                  onClick={this.handleClick}
                >
                  {this.state.buttonTitle}
                </Button>
              </Form.Group>
            </Form.Row>
          </Form>
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { projectname, experimentname } = props.match.params;
  return {
    authd: selectors.getIsAuthenticated(state),
    experiment: selectors.getExperiment(state, projectname, experimentname),
  };
};

export default connect(mapStateToProps)(ExperimentProfile);
