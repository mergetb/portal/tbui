import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Table } from 'react-bootstrap';
import { NavLink, Redirect } from 'react-router-dom';
import * as nas from 'actions/notification_actions.js';
import LoadingCard from 'components/Dialog/LoadingCard';
import AddDeleteItemButton from 'components/CustomButton/CustomButton';
import NewProjectModal from 'components/Dialog/NewProjectModal';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

class ProjectsList extends Component {
  constructor(props) {
    super(props);
    this.getProjectsDataIfNeeded = this.getProjectsDataIfNeeded.bind(this);
    this.showCreateModal = this.showCreateModal.bind(this);
    this.state = {
      openModal: false,
    };
  }

  componentDidMount() {
    this.getProjectsDataIfNeeded();
  }

  componentDidUpdate() {
    this.getProjectsDataIfNeeded();
  }

  getProjectsDataIfNeeded() {
    const { authd, dispatch } = this.props;
    if (authd) {
      dispatch(TbapiActions.fetchProjects());
    }
  }

  handleDelete(projname) {
    const { dispatch } = this.props;
    dispatch(TbapiActions.deleteProject(projname));
    dispatch(nas.SetNotifcation(nas.Level.WARNING, `Deleted project ${projname}`));
  }

  showCreateModal() {
    this.setState((prevState) => ({ openModal: !prevState.openModal }));
  }

  render() {
    const { authd, projects, fetching } = this.props;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    const title = 'Projects';

    if (!projects || fetching) {
      return (
        <LoadingCard title={title} />
      );
    }
    return (
      <>
        <NewProjectModal
          onClose={() => this.showCreateModal()}
          show={this.state.openModal}
        />
        <Card>
          <Card.Header>{title}</Card.Header>
          <Card.Body>
            <Table striped hover>
              <thead>
                <tr>
                  <th>Project Name</th>
                  <th>Description</th>
                  <th>{ }</th>
                </tr>
              </thead>
              <tbody>
                {Object.keys(projects).map((p) => (
                  <tr key={p}>
                    <td key={`${p}.1`}>
                      <NavLink to={`projects/${projects[p].name}`}>
                        {projects[p].name}
                      </NavLink>
                    </td>
                    <td key={`${p}.2`}>{projects[p].description}</td>
                    <td className="tc-actions text-right">
                      <AddDeleteItemButton
                        add={false}
                        item="Project"
                        onClick={() => this.handleDelete(projects[p].name)}
                      />
                    </td>
                  </tr>
                ))}
                <tr>
                  <td className="tc-actions text-left">
                    <AddDeleteItemButton
                      add
                      item="Project"
                      onClick={this.showCreateModal}
                    />
                  </td>
                  <td />
                  <td />
                </tr>
              </tbody>
            </Table>
          </Card.Body>
        </Card>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  authd: selectors.getIsAuthenticated(state),
  projects: selectors.getProjects(state),
  fetching: selectors.isFetchingProjects(state),
});

export default connect(mapStateToProps)(ProjectsList);
