import React, { Component } from 'react';
import {
  Button, Form, FormControl, Modal, Col,
} from 'react-bootstrap';
import AccessMode from 'components/Experiment/AccessMode.jsx';
import * as nas from 'actions/notification_actions';
import * as TbapiActions from 'actions/tbapi_actions';
import { connect } from 'react-redux';

class NewProjectModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projName: { value: '', error: false },
      projDesc: { value: '', error: false },
      accessMode: 'Public',
    };
    this.changeHandler = this.changeHandler.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleModeSelect = this.handleModeSelect.bind(this);
  }

  changeHandler(event) {
    const { name } = event.target;
    const { value } = event.target;

    if (name === 'projName') {
      const re = RegExp('^[a-z][a-z0-9]+$');
      this.setState({
        projName: {
          value,
          error: !re.test(value),
        },
      });
    } else if (name === 'projDesc') {
      this.setState({
        projDesc: {
          value,
          error: false, // empty desc is OK.
        },
      });
    }
  }

  handleModeSelect(choice) {
    this.setState({
      accessMode: choice,
    });
  }

  handleClick() {
    const { Info } = this.props;
    Info(`Creating new project ${this.state.projName.value}`);
    this.props.CreateProject(
      this.state.projName.value,
      this.state.projDesc.value,
      this.state.accessMode.toLowerCase(),
    );
    // Redirect to new project profile? redirect: "/projects/" + this.state.projName,
    this.props.onClose();
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Modal.Header closeButton>Create New Project</Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Row>
              <Form.Group as={Col} md={8}>
                <Form.Label>Name</Form.Label>
                <FormControl
                  label="Name"
                  type="text"
                  name="projName"
                  onChange={this.changeHandler}
                  isInvalid={this.state.projName.error}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') this.handleClick();
                  }}
                />
                <Form.Control.Feedback type="invalid">
                  Project Names must be single word alphanumeric.
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group as={Col} md={4}>
                <AccessMode
                  accessMode={this.state.accessMode}
                  handleSelect={this.handleModeSelect}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Description</Form.Label>
                <FormControl
                  rows="5"
                  as="textarea"
                  placeholder="Enter project description"
                  onChange={this.changeHandler}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col}>
                <Button
                  disabled={this.state.projName.error || this.state.projName.value.length === 0}
                  className="float-right"
                  onClick={this.handleClick}
                  variant="outline-primary"
                >
                  Create Project
                </Button>
              </Form.Group>
            </Form.Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  Info: (message) => dispatch(nas.Info(message)),
  CreateProject: (pid, desc, mode) => dispatch(TbapiActions.createProject(pid, desc, mode)),
});

export default connect((state) => state, mapDispatchToProps)(NewProjectModal);
