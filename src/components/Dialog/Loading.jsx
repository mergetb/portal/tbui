import React from 'react';
import ReactLoading from 'react-loading';

const Loading = () => <ReactLoading height="32px" width="32px" type="bubbles" color="#7fe3f3" />;

export default Loading;
