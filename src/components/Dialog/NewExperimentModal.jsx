import React, { Component } from 'react';
import {
  Modal, Button, Col, Form, FormControl,
} from 'react-bootstrap';
import * as TbapiActions from 'actions/tbapi_actions.js';
import { connect } from 'react-redux';
import * as nas from 'actions/notification_actions.js';

class NewExperimentModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expName: '',
      expDesc: '',
      error: false, // we start with an empty exp name
    };
    this.changeHandler = this.changeHandler.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  changeHandler(event) {
    const { name } = event.target;
    const { value } = event.target;

    if (name === 'expName') {
      const re = RegExp('^[a-z][a-z0-9]+$');
      this.setState({
        expName: value,
        error: !re.test(value),
      });
    } else if (name === 'expDesc') {
      this.setState({
        expDesc: value,
      });
    }
  }

  handleClick() {
    const { CreateExperiment, pid } = this.props;

    CreateExperiment(
      pid,
      this.state.expName,
      this.state.expDesc,
    );

    this.props.onClose();
  }

  render() {
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Modal.Header closeButton>Experiment Profile</Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Row>
              <Form.Group as={Col} md="4">
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  name="expName"
                  value={this.state.expName}
                  onChange={this.changeHandler}
                  isInvalid={this.state.error}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') this.handleClick();
                  }}
                />
                <Form.Control.Feedback type="invalid">
                  Experiment names must be single word alphanumeric.
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group as={Col} md="4">
                <Form.Label>Project</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Project"
                  readOnly
                  value={this.props.pid}
                />
              </Form.Group>
              <Form.Group as={Col} md={4}>
                <Form.Label>Creator</Form.Label>
                <Form.Control
                  type="text"
                  readOnly
                  value={this.props.creator}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col} md="12" controlId="formControlsTextarea">
                <Form.Label>Experiment Description</Form.Label>
                <FormControl
                  rows="5"
                  as="textarea"
                  name="expDesc"
                  placeholder="Write the experiment description here."
                  value={this.state.expDesc}
                  onChange={this.changeHandler}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col}>
                <Button
                  variant="outline-info"
                  disabled={this.state.error || this.state.expName.length === 0}
                  className="float-right"
                  onClick={this.handleClick}
                >
                  Create Experiment
                </Button>
              </Form.Group>
            </Form.Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapStateToProps = () => ({
});

const mapDispatchToProps = (dispatch) => ({
  Info: (message) => dispatch(nas.Info(message)),
  CreateExperiment: (pid, eid, desc) => dispatch(TbapiActions.createExperiment(pid, eid, desc)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewExperimentModal);
