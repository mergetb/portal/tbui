import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Modal, Form, FormGroup, Col,
} from 'react-bootstrap';
import * as TbapiActions from 'actions/tbapi_actions';

class AddPubKeyDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      keyfile: null,
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleModelChange = this.handleModelChange.bind(this);
  }

  handleModelChange(event) {
    this.setState({
      keyfile: event.target.files[0],
    });
  }

  handleClick() {
    const fileReader = new FileReader();
    fileReader.onloadend = () => {
      this.props.addPublicKey(fileReader.result);
    };
    fileReader.readAsText(this.state.keyfile);
    this.props.onSubmit();
  }

  render() {
    const { username } = this.props;
    return (
      <Modal show={this.props.show} onHide={this.props.onClose}>
        <Modal.Header closeButton>
          <Modal.Title>
            {`Add New Public Key for ${username}`}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Row>
              <FormGroup as={Col}>
                <Form.Label>Pubkey File</Form.Label>
                <Form.Control
                  type="file"
                  onChange={this.handleModelChange}
                  onKeyPress={(e) => {
                    if (e.key === 'Enter') this.handleClick();
                  }}
                />
              </FormGroup>
            </Form.Row>
            <Form.Row>
              <FormGroup as={Col}>
                <Button
                  className="float-right"
                  variant="outline-info"
                  disabled={this.state.keyfile === null}
                  onClick={this.handleClick}
                >
                  Add Public Key
                </Button>
              </FormGroup>
            </Form.Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
  }
}

const mapDispatchToProps = (dispatch, props) => ({
  addPublicKey: (contents) => dispatch(TbapiActions.addPublicKey(props.username, contents)),
});

export default connect((state) => state, mapDispatchToProps)(AddPubKeyDialog);
