import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Card, Container, Row, Col, FormGroup, Form, FormControl,
} from 'react-bootstrap';
import AccessMode from 'components/Experiment/AccessMode.jsx';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';
import Loading from 'components/Dialog/Loading';

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.getUserDataIfNeeded = this.getUserDataIfNeeded.bind(this);
  }

  componentDidMount() {
    this.getUserDataIfNeeded();
  }

  componentDidUpdate() {
    this.getUserDataIfNeeded();
  }

  getUserDataIfNeeded() {
    const { isAuthenticated, dispatch } = this.props;
    const { username } = this.props.match.params;

    if (isAuthenticated) {
      dispatch(TbapiActions.fetchUser(username));
    }
  }

  render() {
    const { user, fetching } = this.props;
    const { username } = this.props.match.params;

    // GTL add redirect if not authenticated.

    if (!user || fetching) {
      return (
        <Container fluid>
          {' '}
          <Row>
            {' '}
            <Card key={0} title={username} content={<Loading />} />
            {' '}
          </Row>
          {' '}
        </Container>
      );
    }

    return (
      <Card>
        <Card.Header>{username}</Card.Header>
        <Card.Body>
          <Form>
            <Form.Row>
              <FormGroup as={Col} md={6}>
                <Form.Label>Username</Form.Label>
                <FormControl
                  type="text"
                  placeholder="Username"
                  defaultValue={user.username ? user.username : 'Unknown'}
                  disabled
                />
              </FormGroup>
              <FormGroup as={Col} md={3}>
                <Form.Label>Email Address</Form.Label>
                <FormControl
                  type="email"
                  placeholder="Email"
                  defaultValue={user.email ? user.email : 'Unknown'}
                  disabled
                />
              </FormGroup>
              <FormGroup as={Col} md={3}>
                <AccessMode
                  accessMode={user.accessMode}
                  disabled
                />
              </FormGroup>
            </Form.Row>
            <Form.Row>
              <FormGroup as={Col} md={12}>
                <Form.Label>Name</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Full Name"
                  defaultValue={user.name ? user.name : 'Unknown'}
                  disabled
                />
              </FormGroup>
            </Form.Row>
            <Form.Row>
              <FormGroup as={Col} md={12}>
                <Form.Label>Address</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Street Address"
                  disabled
                />
              </FormGroup>
            </Form.Row>
            <Form.Row>
              <FormGroup as={Col} md={4}>
                <Form.Label>City</Form.Label>
                <FormControl
                  type="text"
                  disabled
                />
              </FormGroup>
              <FormGroup as={Col} md={4}>
                <Form.Label>State</Form.Label>
                <FormControl
                  type="text"
                  disabled
                />
              </FormGroup>
              <FormGroup as={Col} md={4}>
                <Form.Label>Postal Code</Form.Label>
                <FormControl
                  type="text"
                  disabled
                />
              </FormGroup>
            </Form.Row>
            <Form.Row>
              <FormGroup as={Col} md={12}>
                <Form.Label>About Me</Form.Label>
                <FormControl
                  cols={10}
                  as="textarea"
                  placeholder="The profile description goes here..."
                  disabled
                />
              </FormGroup>
            </Form.Row>
            <Button
              variant="outline-info"
              type="submit"
              className="float-right"
              disabled
            >
              Update Profile
            </Button>
          </Form>
        </Card.Body>
      </Card>
    ); // end of return ( )
  } // end of render();
}

const mapStateToProps = (state, props) => ({
  isAuthenticated: selectors.getAuth(state).isAuthenticated,
  user: selectors.getUser(state, props.match.params.username),
  fetching: selectors.isFetchingUser(state, props.match.params.username),
});

export default connect(mapStateToProps)(UserProfile);
