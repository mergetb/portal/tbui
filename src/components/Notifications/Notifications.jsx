import React, { Component } from 'react';
import {
  Button, Card, ListGroup, ListGroupItem, ButtonToolbar, Dropdown, DropdownButton,
} from 'react-bootstrap';
import { connect } from 'react-redux';
import * as selectors from 'reducers/selectors.js';
import * as nas from 'actions/notification_actions.js';

class Notifications extends Component {
  static level2Style(l) {
    switch (l) {
      case nas.Level.DEBUG: return 'success';
      case nas.Level.INFO: return 'info';
      case nas.Level.WARNING: return 'warning';
      case nas.Level.ERROR: return 'danger';
      case nas.Level.CRITICAL: return 'danger';
      default: return 'success';
    }
  }

  static level2Icon(l) {
    switch (l) {
      case nas.Level.DEBUG: return 'fa fa-bug';
      case nas.Level.INFO: return 'fa fa-info';
      case nas.Level.WARNING: return 'fa fa-exclamation-circle';
      case nas.Level.ERROR: return 'fa fa-bomb';
      case nas.Level.CRITICAL: return 'fa fa-skull-crossbones';
      default: return 'success';
    }
  }

  constructor(props) {
    super(props);
    this.clearAll = this.clearAll.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      filter: null,
    };
  }

  handleClick(item) {
    const { dispatch } = this.props;
    dispatch(nas.ClearNotification(item));
  }

  clearAll() {
    // There is probably a much better way to do this in react or css or className.
    const { dispatch } = this.props;
    dispatch(nas.ClearAllNotifications());
  }

  filter(level) {
    if (level === 'None') {
      this.setState({ filter: null });
    } else {
      this.setState({ filter: level });
    }
  }

  render() {
    const { notifications } = this.props;
    if (!notifications) {
      return null;
    }
    const levels = [
      nas.Level.DEBUG, nas.Level.INFO, nas.Level.WARNING, nas.Level.ERROR,
      nas.Level.CRITICAL, 'None',
    ];

    const menu = (
      <ButtonToolbar className="float-right">
        <Button size="sm" variant="outline-primary" onClick={this.clearAll}>Clear All</Button>
        <DropdownButton id="1" title="Filter" size="sm" variant="outline-primary">
          {levels.map((level) => (
            <Dropdown.Item
              key={level}
              eventKey={level}
              onSelect={() => this.filter(level)}
            >
              {level}
            </Dropdown.Item>
          ))}
        </DropdownButton>
      </ButtonToolbar>
    );
    const items = notifications.map((item, key) => {
      if (!this.state.filter || (this.state.filter && this.state.filter === item.level)) {
        return (
          <ListGroupItem
            key={key} // eslint-disable-line react/no-array-index-key
            variant={Notifications.level2Style(item.level)}
            onClick={() => this.handleClick(item)}
          >
            <div>
              <span className="pull-right text-muted">
                {new Date(item.timestamp).toLocaleTimeString()}
              </span>
            </div>
            <div>
              <i className={Notifications.level2Icon(item.level)} />
              [
              {item.level}
              ]
              {' '}
              {item.message}
            </div>
          </ListGroupItem>
        );
      }
      // else
      return null;
    });

    return (
      <Card>
        <Card.Header>
          {'Notifications'}
          {menu}
        </Card.Header>
        <Card.Body>
          <ListGroup>
            {items}
          </ListGroup>
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  notifications: selectors.getNotifications(state),
});

export default connect(mapStateToProps)(Notifications);
