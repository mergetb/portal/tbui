import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Button, Card, ButtonToolbar } from 'react-bootstrap';
import Accordion from 'react-bootstrap/Accordion';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';
import RealizationMapping from 'components/Realization/RealizationMapping.jsx';

class RealizationAction extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disableChoice: false,
      rejected: false,
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(accept) {
    const {
      pid, eid, rid, acceptRlz,
    } = this.props;
    acceptRlz(accept, pid, eid, rid);
    this.setState({
      disableChoice: true,
      rejected: !accept,
    });
    this.props.accepted(accept);
  }

  render() {
    const {
      authd, rlz, pid, eid,
    } = this.props;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    if (this.state.rejected) {
      // If we rejected the rlz, redirect to the experiment page as this rlz page
      // is no longer active as the rlz does not exist.
      const path = `/projects/${pid}/experiments/${eid}`;
      return <Redirect to={path} />;
    }

    const expires = Date.parse(rlz.expires);
    let { disableChoice } = this.state;
    if (expires <= 0) {
      disableChoice = true;
    }
    return (
      <Accordion defaultActiveKey="0">
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="0">Realization Mapping</Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              { disableChoice
                ? (
                  null
                )
                : (
                  <ButtonToolbar>
                    <Button
                      variant="outline-info"
                      onClick={() => this.handleClick(true)}
                    >
                      Accept
                    </Button>
                    <Button
                      variant="outline-danger"
                      onClick={() => this.handleClick(false)}
                    >
                      Reject
                    </Button>
                  </ButtonToolbar>
                )}
              <RealizationMapping rlz={rlz} />
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { pid, eid, rid } = props;
  return {
    authd: selectors.getIsAuthenticated(state),
    rlz: selectors.getRealization(state, pid, eid, rid),
  };
};

const mapDispatchToProps = (dispatch) => ({
  acceptRlz: (val, pid, eid, rid) => dispatch(
    TbapiActions.realizationAcceptAction(val, pid, eid, rid),
  ),
});

export default connect(mapStateToProps, mapDispatchToProps)(RealizationAction);
