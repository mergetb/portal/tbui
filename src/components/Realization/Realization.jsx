import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, NavLink } from 'react-router-dom';
import {
  Card, Table, Container, Row, Col,
} from 'react-bootstrap';

import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

// import RealizationTopology from "components/Model/RealizationTopology";
import RealizationAction from 'components/Realization/RealizationAction';
import MaterializationView from 'components/Materialization/MaterializationView';
import ExperimentVersion from 'components/Experiment/ExperimentVersion';
import LoadingCard from 'components/Dialog/LoadingCard';

class Realization extends Component {
  constructor(props) {
    super(props);
    this.getDataIfNeeded = this.getDataIfNeeded.bind(this);
    this.state = {
      expires_in: -1,
    };
    this.accepted = this.accepted.bind(this);
  }

  componentDidMount() {
    this.getDataIfNeeded();
    this.timerID = setInterval(() => this.checkExpire(), 1000);
  }

  componentDidUpdate() {
    this.getDataIfNeeded();
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  getDataIfNeeded() {
    const {
      authd, pid, eid, rid,
    } = this.props;
    if (authd) {
      this.props.fetchRealization(pid, eid, rid);
    }
  }

  checkExpire() {
    const { real } = this.props;
    if (!real) {
      return;
    }
    const expAt = Date.parse(real.expires);
    if (expAt < -62135596800) { // realization already accepted. date is very much less than
      // Jan 1st 1970.
      clearInterval(this.timerID);
      this.setState({ expires_in: -1 });
      return;
    }
    const remaining = Math.round((expAt - Date.now()) / 1000);
    if (remaining <= 0) {
      clearInterval(this.timerID);
    }
    this.setState({
      expires_in: remaining <= 0 ? 0 : remaining,
    });
  }

  accepted(accept) {
    const {
      pid, eid, rid,
    } = this.props;

    clearInterval(this.timerID);
    if (accept) this.setState({ expires_in: -1 });
    else this.setState({ expires_in: 0 });

    // refresh realization date with new timestamp
    this.props.fetchRealization(pid, eid, rid);
  }

  render() {
    const {
      real, fetching, authd, pid, eid, rid,
    } = this.props;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    if (!real || fetching) {
      return (
        <LoadingCard title="Realization Information" subtitle="loading..." />
      );
    }

    let expire; let
      expireClass;
    if (this.state.expires_in === 0) {
      expire = 'Expired';
      expireClass = 'text-danger';
    } else if (this.state.expires_in === -1) {
      expire = 'Never';
      expireClass = 'text-success';
    } else {
      expire = this.state.expires_in;
      if (expire <= 10) {
        expireClass = 'text-danger text-bold';
      } else {
        expireClass = 'text-warning';
      }
    }
    const minHash = real.xhash.slice(0, 8);
    return (
      <Container fluid>
        <Row>
          <Col md={6}>
            <Card>
              <Card.Header>Realization Information</Card.Header>
              <Card.Body>
                <Table hover>
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Expires</th>
                      <th>Complete</th>
                      <th>Experiment</th>
                      <th>Version</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{rid}</td>
                      <td className={expireClass}>{expire}</td>
                      <td>{real.complete ? 'True' : 'False'}</td>
                      <td>
                        <NavLink to={`/projects/${pid}/experiments/${eid}`}>
                          {eid}
                        </NavLink>
                      </td>
                      <td>
                        <NavLink to={`/projects/${pid}/experiments/${eid}/src/${real.xhash}`}>
                          {minHash}
                        </NavLink>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
            <ExperimentVersion
              projectname={pid}
              match={{
                params: {
                  experimentname: eid,
                  projectname: pid,
                  xhash: real.xhash,
                },
              }}
            />
          </Col>
          <Col md={6}>
            <RealizationAction
              accepted={this.accepted}
              pid={pid}
              eid={eid}
              rid={rid}
            />
            <MaterializationView
              pid={pid}
              eid={eid}
              rid={rid}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { pid, eid, rid } = props;
  return {
    authd: selectors.getIsAuthenticated(state),
    real: selectors.getRealization(state, pid, eid, rid),
    fetching: selectors.isFetchingRealization(state, pid, eid, rid),
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchRealization: (pid, eid, rid) => dispatch(TbapiActions.fetchRealization(pid, eid, rid)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Realization);
