import React, { Component } from 'react';
import { Table } from 'react-bootstrap';

class RealizationMapping extends Component {
  render() {
    const { rlz } = this.props;

    // Sample rlz.Nodes:
    // "nodes": {
    //   "a": {
    //     "poolSize": 509,
    //     "selected": "m990",
    //     "xlabel": "a",
    //     "rlabel": "m990",
    //     "site": "site.dcomptb.net"
    //   },
    //
    if (!Object.hasOwnProperty.call(rlz, 'nodes')) {
      return null;
    }

    return (
      <Table striped bordered>
        <thead>
          <tr>
            <th>Node</th>
            <th>Resource</th>
            <th>Site</th>
          </tr>
        </thead>
        <tbody>
          {
            Object.keys(rlz.nodes).map((n) => (
              <tr key={n}>
                <td>{rlz.nodes[n].xlabel}</td>
                <td>{rlz.nodes[n].rlabel}</td>
                <td>{rlz.nodes[n].site}</td>
              </tr>
            ))
          }
        </tbody>
      </Table>
    );
  }
}

export default RealizationMapping;
