import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';

class Footer extends Component {
  render() {
    return (
      <Navbar bg="light" variant="light">
        <Nav className="mr-auto">
          <Nav.Item>
            <Nav.Link
              href="https://mergetb.slack.com"
              rel="noopener noreferrer"
              target="_blank"
            >
              Slack
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link
              href="https://gitlab.com/mergetb/tbui/issues"
              rel="noopener noreferrer"
              target="_blank"
            >
              Bug Report
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link
              href="https://status.mergetb.net"
              rel="noopener noreferrer"
              target="_blank"
            >
              Testbed Status
            </Nav.Link>
          </Nav.Item>
        </Nav>
        <Nav>
          <Nav.Item>
            <p className="copyright">
              &copy;
              {' '}
              {new Date().getFullYear()}
              {' '}
              <a href="https://www.mergetb.org">Merge TB</a>
            </p>
          </Nav.Item>
        </Nav>
      </Navbar>
    );
  }
}

export default Footer;
