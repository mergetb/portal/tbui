import React, { Component } from 'react';
import Loading from 'components/Dialog/Loading';
import * as TbapiActions from 'actions/tbapi_actions.js';
import CustomAlert from 'components/Dialog/CustomAlert';
import RealizationTopology from 'components/Model/RealizationTopology';

class ModelVisualization extends Component {
  constructor(props) {
    super(props);

    this.state = {
      xir: null,
      alertheader: '',
      alertmessage: '',
    };
  }

  componentDidMount() {
    this.loadTopology();
  }

  static getDerivedStateFromError(error) {
    return {
      alertheader: 'JS Error',
      alertmessage: error.message,
      hasError: true,
    };
  }

  loadTopology() {
    TbapiActions.fetchTopology(this.props.xir)
      .then(
        (results) => {
          if (results.ok) {
            return results.json();
          }
          results.text().then((text) => {
            this.setState({
              alertheader: 'Compile Error',
              alertmessage: text,
            });
          });
          return Promise.resolve();
        },
        (error) => {
          // GTL handle error. Pop up error message?
          this.setState({
            alertheader: 'Connection Error',
            alertmessage: error.message,
          });
          return Promise.reject();
        },
      )
      .then((data) => {
        if (data) {
          const x = JSON.parse(data.xir);
          this.setState({
            alertheader: '',
            alertmessage: '',
            xir: x,
          });
        }
      });
  }

  render() {
    let alert = null;
    let body = null;
    const title = `Model: ${this.props.name}`;

    if (this.state.hasError) {
      return <CustomAlert show message={this.state.alertmessage} header={this.state.alertheader} />;
    }

    if (this.state.alertmessage !== '') {
      alert = (
        <CustomAlert
          alertstyle="danger"
          show={this.state.alertmessage !== ''}
          message={this.state.alertmessage}
          header={this.state.alertheader}
        />
      );
    } else if (this.state.xir !== null) {
      body = (
        <RealizationTopology
          title={title}
          topology={this.state.xir}
        />
      );
    } else {
      body = <Loading />;
    }
    return (
      <>
        {alert}
        {body}
      </>
    );
  }
}

export default ModelVisualization;
