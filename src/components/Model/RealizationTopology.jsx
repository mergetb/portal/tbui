import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import D3RealizationTopology from 'components/Model/D3RealizationTopology';

class RealizationTopology extends Component {
  constructor(props) {
    super(props);
    this.topo = null;
  }

  componentDidMount() {
    this.topo = new D3RealizationTopology();
    this.topo.create(this.el, this.props.topology);
  }

  componentDidUpdate() {
    // Do not let React update this element
    return false;
  }

  componentWillUnmount() {
    if (this.topo) {
      D3RealizationTopology.destroy(this.el);
    }
  }

  render() {
    const { nodes, links } = this.props.topology;
    const { title = 'Topology' } = this.props;
    return (
      <Card>
        <Card.Header>
          {title}
          <br />
          <small className="text-muted">
            {`${nodes.length} nodes & ${links ? links.length : 0} links`}
          </small>
        </Card.Header>
        <Card.Body>
          <div
            className="mtb_model_topo_container"
            ref={(el) => this.el = el} // eslint-disable-line no-return-assign
          />
        </Card.Body>
      </Card>
    );
  }
}

export default RealizationTopology;
