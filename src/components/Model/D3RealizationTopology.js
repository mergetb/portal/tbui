/* eslint-disable camelcase, no-param-reassign */
import * as d3 from 'd3';
import { xir2graph, graph2d3force } from 'lib/graph';

class D3RealizationTopology {
  constructor() {
    this.sim = null;
    this.nodes = [];
    this.links = [];

    this.forceStrength = 0.25;

    this.ticked = this.ticked.bind(this);
    this.drag_node = this.drag_node.bind(this);
    this.node_charge = this.node_charge.bind(this);

    this.node_color = this.node_color.bind(this);
    this.node_stroke = this.node_stroke.bind(this);

    this.color = d3.scaleOrdinal(d3.schemeCategory10);
    this.default_node_color = 'lightblue';
  }

  create(el, topo) {
    this.el = el;

    const t = graph2d3force(xir2graph(topo));
    this.nodes = JSON.parse(JSON.stringify(t.nodes));
    this.links = JSON.parse(JSON.stringify(t.links));

    this.sim = d3.forceSimulation()
      .nodes(this.nodes)
      .force('charge', d3.forceManyBody().strength(this.node_charge))
      .force('center', d3.forceCenter(el.clientWidth / 2, el.clientHeight / 2))
      .force('link', d3.forceLink(this.links))
      .force('x', d3.forceX())
      .force('y', d3.forceY())
      .on('tick', this.ticked)
      .stop();

    // graph will live in this g
    const svg = d3.select(el).append('svg');

    svg.attr('height', el.clientHeight)
      .attr('width', el.clientWidth);

    svg.call(d3.zoom().on('zoom', this.zoom.bind(this)));
    svg.append('g');

    this.sim.force('center', d3.forceCenter(el.clientWidth / 2, el.clientHeight / 2));
    this.update(el, topo);
  }

  update(el) {
    this.el = el;

    const g = d3.select(el).select('svg').select('g');

    const lineSel = g.selectAll('line').data(this.links, (d) => d.name);
    lineSel.exit().remove();
    const lineEnt = lineSel.enter();
    lineEnt
      .append('line')
      .classed('link', true)
      .attr('stroke-width', 1)
      .attr('stroke', (d) => d.props.color);

    const nodeSel = g.selectAll('path').data(this.nodes, (d) => d.name);
    nodeSel.exit().remove();

    const nodeEnt = nodeSel.enter().append('g').classed('node', true);
    nodeEnt
      .append('path')
      .attr('d', d3.symbol()
        .size((d) => Math.PI ** d.props.size || 300, 2)
        .type((d) => {
          switch (d.props.shape) {
            case 'circle': return d3.symbolCircle;
            case 'square': return d3.symbolSquare;
            case 'cross': return d3.symbolCross;
            case 'diamond': return d3.symbolDiamond;
            case 'star': return d3.symbolStar;
            case 'triangle': return d3.symbolTriangle;
            case 'wye': return d3.symbolWye;
            default: return d3.symbolCircle;
          }
        }))
      .style('fill', (d) => this.node_color(d))
      .style('stroke', (d) => this.node_stroke(d))
      .style('stroke-width', 2);

    nodeEnt
      .append('text')
      .attr('dx', 12)
      .attr('dy', '.35em')
      .text((d) => d.name);

    nodeEnt.call(d3.drag()
      .on('start', this.drag_node.bind(this, 'start'))
      .on('drag', this.drag_node.bind(this, 'drag'))
      .on('end', this.drag_node.bind(this, 'end')));

    this.sim.alpha(1).restart();
  }

  static destroy(el) {
    d3.select(el).selectAll('*').remove();
  }

  zoom() {
    d3.select(this.el).select('g').attr('transform', d3.event.transform);
  }

  ticked() {
    const g = d3.select(this.el);
    g.selectAll('.node')
      .attr('transform', (d) => `translate(${d.x},${d.y})`);

    g.selectAll('.link')
      .attr('x1', (link) => link.source.x)
      .attr('y1', (link) => link.source.y)
      .attr('x2', (link) => link.target.x)
      .attr('y2', (link) => link.target.y);
  }

  drag_node(action, d) {
    switch (action) {
      case 'start':
        if (!d3.event.active) {
          this.sim.alphaTarget(0.09).restart();
        }
        d.fx = d.x;
        d.fy = d.y;
        break;
      case 'drag':
        d.fx = d3.event.x;
        d.fy = d3.event.y;
        break;
      case 'end':
        if (!d3.event.active) {
          this.sim.alphaTarget(0);
        }
        d.fx = null;
        d.fy = null;
        break;
      default:
        break;
    }
  }

  node_color(d) {
    const c = Object.hasOwnProperty.call(d.props, 'color') ? d.props.color : '';
    const g = Object.hasOwnProperty.call(d.props, 'group') ? d.props.group : 0;

    if (c) return c;
    if (g) return this.color(g);
    return this.default_node_color;
  }

  node_stroke(d) {
    const c = Object.hasOwnProperty.call(d.props, 'color') ? d.props.color : '';
    const g = Object.hasOwnProperty.call(d.props, 'group') ? d.props.group : 0;

    if (c && g) { // color and group, use group as stroke as color is fill
      return d3.rgb(this.color(g)).darker();
    }

    if (c && !g) { // just color, use color as stroke
      return d3.rgb(c).darker();
    }

    if (!c && g) { // just group, use group
      return d3.rgb(this.color(g)).darker();
    }

    // no color and no group - use default color as stroke
    return d3.rgb(this.default_node_color).darker();
  }

  node_charge() {
    return -this.forceStrength * (40 ** 2.0);
  }
}

export default D3RealizationTopology;
