import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as selectors from 'reducers/selectors.js';

import './NotFound.css';

class NotFound extends Component {
  render() {
    if (!this.props.authd) {
      return <Redirect to="/landing" />;
    }
    return <Redirect to="/" />;
  }
}

const mapStateToProps = (state) => ({
  authd: selectors.getIsAuthenticated(state),
});
export default connect(mapStateToProps)(NotFound);
