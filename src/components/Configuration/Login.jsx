import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Nav, NavDropdown } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import Loading from 'components/Dialog/Loading';
import * as selectors from 'reducers/selectors';
import * as AuthActions from 'actions/auth_actions';
import * as TbapiActions from 'actions/tbapi_actions';
import Auth from 'lib/auth';

class Login extends Component {
  constructor(props) {
    super(props);
    this.doAuthentication = this.doAuthentication.bind(this);
    this.refreshData = this.refreshData.bind(this);
    this.doLogout = this.doLogout.bind(this);
    this.state = {
      loading: false,
      loaded: false,
    };
  }

  componentDidMount() {
    this.refreshData();
  }

  componentDidUpdate(prevProps) {
    const { profile } = this.props;
    if (profile !== prevProps.profile) {
      if (profile) {
        if (Object.hasOwnProperty.call(profile, 'nickname')) { // profile finished loading.
          // Load the user. This will correctly set userIsActive
          this.props.fetchUser(this.props.profile.nickname);
          this.setState({ // eslint-disable-line react/no-did-update-set-state
            loading: false,
            loaded: true,
          });
        } else {
          this.setState({ // eslint-disable-line react/no-did-update-set-state
            loaded: false,
            loading: false,
          });
        }
      }
    }
  }

  refreshData() {
    if (Auth.isAuthenticated()) {
      this.props.loadAuth();
      if (!this.state.loaded) {
        if (!this.state.loading) {
          this.setState({ loading: true });
          this.props.getAuthProfile();
        }
      }
    }
  }

  doAuthentication() {
    this.props.authLogin();
  }

  doLogout() {
    this.setState({
      loading: false,
      loaded: false,
    });
    this.props.authLogout();
  }

  render() {
    const { profile } = this.props;

    let title = (<Loading />);
    let name = 'User';
    if (profile && profile.nickname) {
      name = profile.nickname;
    }
    if (this.state.loaded) {
      title = (
        <span>
          {name}
          <img
            src={profile && profile.picture ? profile.picture : null}
            alt="Profile"
            className="nav-user-profile rounded-circle"
            width="30"
          />
        </span>
      );
    }
    if (Auth.isAuthenticated()) {
      return (
        <NavDropdown title={title} id="basic-nav-dropdown">
          {this.state.loaded
              && (
                <NavDropdown.Item as={NavLink} to={`/users/${name}`}>
                  Profile
                </NavDropdown.Item>
              )}
          <NavDropdown.Item onClick={this.doLogout}>
            Log Out
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/configuration">
            Configuration
          </NavDropdown.Item>
        </NavDropdown>
      );
    }

    // else not isAuthenticated
    return (
      <Nav.Link onClick={this.doAuthentication}>
        Log in
      </Nav.Link>
    );
  }
}

const mapStateToProps = (state) => ({
  profile: selectors.getAuthdProfile(state),
  tokenExpired: selectors.getIsTokenExpired(state),
});

const mapDispatchToProps = (dispatch) => ({
  authLogout: () => dispatch(AuthActions.logout()),
  authLogin: () => dispatch(AuthActions.login()),
  getAuthProfile: () => dispatch(AuthActions.getProfile()),
  loadAuth: () => dispatch(AuthActions.loginSuccess()),
  fetchUser: (uid) => dispatch(TbapiActions.fetchUser(uid)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
