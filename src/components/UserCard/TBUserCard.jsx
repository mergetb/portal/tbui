import React, { Component } from 'react';
import { connect } from 'react-redux';
import Button from 'components/CustomButton/CustomButton.jsx';

import defAvatar from 'assets/img/faces/face-3.jpg';

class TBUserCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bgImage: 'https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400',
    };
  }

  render() {
    const { profile } = this.props;
    const avatar = Object.hasOwnProperty.call(profile, 'avatar') ? profile.avatar : defAvatar;

    return (
      <div className="card card-user">
        <div className="image">
          <img src={this.state.bgImage} alt="..." />
        </div>
        <div className="content">
          <div className="author">
            <a href="#author">
              <img
                className="avatar border-gray"
                src={avatar}
                alt="..."
              />
              <h4 className="title">
                {profile.name}
                <br />
                <small>{profile.username}</small>
                <br />
                <small>{profile.email}</small>
              </h4>
            </a>
          </div>
          <p className="description text-center">{profile.about ? profile.about : ''}</p>
        </div>
        <hr />
        <div>
          <center>
            <Button simple>
              <i className="fa fa-facebook-square" />
            </Button>
            <Button simple>
              <i className="fa fa-twitter" />
            </Button>
            <Button simple>
              <i className="fa fa-google-plus-square" />
            </Button>
          </center>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  profile: state.user,
});

export default connect(mapStateToProps)(TBUserCard);
