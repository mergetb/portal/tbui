import React, { Component } from 'react';
import { connect } from 'react-redux';

class AuthUserCard extends Component {
  render() {
    const { profile } = this.props;

    return (
      <div className="card card-user">
        <div className="content">
          <div className="author">
            <a href="#author">
              <img
                className="avatar border-gray"
                src={profile.picture}
                alt="..."
              />
              <h4 className="title">
                {profile.name}
                <br />
                <small>{profile.nickname}</small>
                <br />
                <small>{profile.email}</small>
              </h4>
            </a>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  profile: state.auth.profile,
});

export default connect(mapStateToProps)(AuthUserCard);
