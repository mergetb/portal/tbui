import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Button, Card, Row, Col, FormGroup, Form, FormControl,
} from 'react-bootstrap';
import { Redirect } from 'react-router-dom';

import AccessMode from 'components/Experiment/AccessMode.jsx';
import * as nas from 'actions/notification_actions.js';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors.js';

import LoadingCard from 'components/Dialog/LoadingCard';

class ProjectProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonTitle: this.props.create ? 'Create Project' : 'Update Project',
      buttonDisabled: true,
      buttonAction: this.props.create ? 'create' : 'update',
      initialName: this.props.match.params.projectname,
      projName: this.props.match.params.projectname,
      projDesc: '', // filled in when we goet it (get/post) or when the user enters it (put)
      redirect: null,
      accessMode: 'Public',
    };
    this.getProjectData = this.getProjectData.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleDescChange = this.handleDescChange.bind(this);
    this.handleModeSelect = this.handleModeSelect.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
  }

  componentDidMount() {
    this.getProjectData();
  }

  componentDidUpdate() {
    this.getProjectData();
  }

  getProjectData() {
    const { authd } = this.props;
    const { projectname } = this.props.match.params;

    if (authd) {
      this.props.FetchProject(projectname); // will only fetch if needed.
    }
  }

  getNameValidationState() {
    // no spaces and force lowercase [a-z]. numerics are allowed, but portal
    // needs to be patched to support this.
    const re = RegExp('^[a-z][a-z0-9]+$');
    if (this.state.projName && !re.test(this.state.projName)) {
      return 'error';
    }
    return 'success';
  }

  handleDescChange(event) {
    this.setState({
      buttonDisabled: false,
      projDesc: event.target.value,
    });
  }

  handleNameChange(event) {
    let newText = 'Create Project';
    let action = 'create';
    if (event.target.value === this.state.initialName) {
      newText = 'Update Project';
      action = 'update';
    }
    const disabled = this.getNameValidationState() === 'error';
    this.setState({
      buttonTitle: newText,
      buttonDisabled: disabled,
      projName: event.target.value,
      buttonAction: action,
    });
  }

  handleModeSelect(choice) {
    this.setState(
      (prevState) => ({
        accessMode: choice,
        buttonDisabled: prevState.accessMode === choice,
      }),
    );
  }

  handleClick() {
    const { Info } = this.props;
    if (this.state.buttonAction === 'update') {
      Info(`Updating project ${this.state.projName}`);
      this.props.UpdateProject(
        this.state.projName,
        this.state.projDesc,
        this.state.accessMode.toLowerCase(),
      );
    } else {
      Info(`Creating new project ${this.state.projName}`);
      this.props.CreateProject(
        this.state.projName,
        this.state.projDesc,
        this.state.accessMode.toLowerCase(),
      ).then(() => {
        this.setState(
          (prevState) => ({
            initialName: prevState.projName,
            redirect: `/projects/${prevState.projName}`,
          }),
        );
      });
    }
    this.setState({
      buttonDisabled: true,
    });
  }

  render() {
    const { fetching, create, authd } = this.props;
    const { projectname } = this.props.match.params;
    let { project } = this.props;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />;
    }

    if (!create) {
      if (!project || fetching) {
        const title = `Loading project profile for ${projectname}`;
        return (
          <LoadingCard key={0} title={title} />
        );
      }
    } else {
      project = {
        name: '',
        description: '',
      };
    }
    return (
      <Card>
        <Card.Header>Profile</Card.Header>
        <Card.Body>
          <Form>
            <Row className="align-items-end">
              <Col md={8}>
                <FormGroup>
                  <Form.Label>Name</Form.Label>
                  <FormControl
                    label="Projectname"
                    type="text"
                    bsPrefix="form-control"
                    placeholder="Project Name"
                    defaultValue={project.name}
                    onChange={this.handleNameChange}
                    onKeyPress={(e) => {
                      if (e.key === 'Enter') this.handleClick();
                    }}
                  />
                </FormGroup>
              </Col>
              <Col md={4}>
                <AccessMode
                  accessMode={project.accessMode}
                  handleSelect={this.handleModeSelect}
                />
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup controlId="formControlsTextarea">
                  <Form.Label>Description</Form.Label>
                  <FormControl
                    rows="5"
                    as="textarea"
                    bsPrefix="form-control"
                    placeholder="Enter project description"
                    defaultValue={project.description}
                    onChange={this.handleDescChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  className="float-right"
                  disabled={this.state.buttonDisabled}
                  onClick={this.handleClick}
                  variant="outline-primary"
                >
                  {this.state.buttonTitle}
                </Button>
              </Col>
            </Row>
          </Form>
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { projectname } = props.match.params;
  return {
    authd: selectors.getIsAuthenticated(state),
    project: selectors.getProject(state, projectname),
    fetching: selectors.isFetchingProject(state, projectname),
  };
};

const mapDispatchToProps = (dispatch) => ({
  Info: (message) => dispatch(nas.Info(message)),
  FetchProject: (name) => dispatch(TbapiActions.fetchProject(name)),
  UpdateProject: (pid, desc, mode) => dispatch(TbapiActions.updateProject(pid, desc, mode)),
  CreateProject: (pid, desc, mode) => dispatch(TbapiActions.createProject(pid, desc, mode)),
});


export default connect(mapStateToProps, mapDispatchToProps)(ProjectProfile);
