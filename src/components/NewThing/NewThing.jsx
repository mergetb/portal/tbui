import React, { Component } from 'react';
import NotFound from 'components/NotFound/NotFound';
import AddProjectMember from 'components/MembersList/AddProjectMember.jsx';

class NewThing extends Component {
  render() {
    const {
      thing, arg1,
    } = this.props.match.params;
    // GTL Ugly! react-router mentions you can pass "state" via a Link, which is how we get here,
    // but I couldn't get it to work after a brief try...
    delete this.props.match.params.thing;
    delete this.props.match.params.arg1;

    switch (thing) {
      case 'projectmember':
        this.props.match.params.projectname = arg1;
        return <AddProjectMember {...this.props} />;
      default:
        return <NotFound />;
    }
  }
}

export default NewThing;
