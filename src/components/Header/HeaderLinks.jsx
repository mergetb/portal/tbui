import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Nav, Badge } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import * as selectors from 'reducers/selectors.js';
import dashboardRoutes from 'routes/dashboard';
import Login from 'components/Configuration/Login';

class HeaderLinks extends Component {
  constructor(props) {
    super(props);
    this.toggleNotifications = this.toggleNotifications.bind(this);
  }

  toggleNotifications() {
    if (this.props.toggleNotification) this.props.toggleNotification();
  }

  render() {
    const { notifications, authd, userActive } = this.props;

    const numNots = notifications.length;

    const links = [];
    for (let i = 0; i < dashboardRoutes.length; i += 1) {
      const r = dashboardRoutes[i];
      if (Object.hasOwnProperty.call(r, 'header') && r.header === true) {
        if (Object.hasOwnProperty.call(r, 'authentication') && r.authentication === true) {
          if (authd === true && userActive === true) {
            links.push(
              <Nav.Item key={r.name}>
                <Nav.Link as={NavLink} to={r.path}>{r.name}</Nav.Link>
              </Nav.Item>,
            );
          }
        } else { // auth not needed
          links.push(
            <Nav.Item key={r.name}>
              <Nav.Link as={NavLink} to={r.path}>{r.name}</Nav.Link>
            </Nav.Item>,
          );
        }
      }
    }

    return (
      <>
        <Nav className="mr-auto">
          {links.length !== 0 ? links : <Nav.Item /> }
        </Nav>
        <Nav className="ml-auto">
          <Nav.Item>
            <Nav.Link eventKey={3} onClick={this.toggleNotifications}>
              {'Notifications '}
              {numNots === 0 ? null : <Badge pill variant="primary">{numNots}</Badge>}
            </Nav.Link>
          </Nav.Item>
          <Login />
        </Nav>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  notifications: selectors.getNotifications(state),
  authd: selectors.getIsAuthenticated(state),
  userActive: selectors.getUserActive(state),
});

export default connect(mapStateToProps)(HeaderLinks);
