import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card } from 'react-bootstrap';
import ReactJson from 'react-json-view';

class ShowReduxState extends Component {
  render() {
    if (process.env.NODE_ENV !== 'development') {
      return null;
    }
    const { state } = this.props;
    return (
      <Card>
        <Card.Header>react-redux state</Card.Header>
        <Card.Body>
          <ReactJson src={state} enableClipboard={false} />
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state) => ({
  state,
});

export default connect(mapStateToProps)(ShowReduxState);
