/* eslint no-console: 0 */
/* eslint react/no-array-index-key: 0 */
import React, { Component } from 'react';

import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as TbapiActions from 'actions/tbapi_actions.js';
import * as selectors from 'reducers/selectors';
import Loading from 'components/Dialog/Loading.jsx';
import Accordion from 'react-bootstrap/Accordion';
import {
  Card, Button, Table,
} from 'react-bootstrap';

class MaterializationView extends Component {
  static tasksComplete(data) {
    if (!data.status || !data.status.siteTasks) return false;

    // check each task for each site. it any are complete===false, we are not complete.
    // Note that lack of a "complete" key means the task is not complete.
    const sites = Object.keys(data.status.siteTasks);
    for (let i = 0; i < sites.length; i += 1) {
      const { tasks } = data.status.siteTasks[sites[i]];
      if (tasks) {
        for (let t = 0; t < tasks.length; t += 1) {
          if (!Object.prototype.hasOwnProperty.call(tasks[t], 'complete') || tasks[t].complete !== true) {
            return false;
          }
        }
      }
    }
    return true;
  }

  constructor(props) {
    super(props);
    this.handleClickMtz = this.handleClickMtz.bind(this);
    this.pollForStatus = this.pollForStatus.bind(this);
    this.startStatusPoll = this.startStatusPoll.bind(this);
    this.timerId = null;

    this.state = {
      inTransition: false,
    };
  }

  componentDidMount() {
    if (this.props.authd) {
      this.props.getMtz();
    }
  }

  componentDidUpdate(prevProps) {
    const { status } = this.props;

    // state change in mtz. either start poll or stop poll.
    if (prevProps.mtz !== this.props.mtz) {
      if (!this.props.mtz) {
        // we had a mtz, but now it's gone.
        // console.log('cancelling poll timer as mtz disapeared');
        this.stopStatusPoll();
      } else {
        // we didn't have a mtz, but now we do. So start checking the status.
        console.log('new mtz, starting poll timer');
        this.startStatusPoll();
      }
    }

    // If the current status is complete, cancel the timer.
    if (prevProps.status !== status) {
      if (!status) {
        // We had a status and it went away. Demtz complete.
        // console.log('cancelling poll timer as status has disapeared (demtz)');
        this.stopStatusPoll();
        // force reread of mtz.
        this.props.getMtz();
      } else {
        // existing status has updated. check completion status.
        if (MaterializationView.tasksComplete(status)) { // eslint-disable-line no-lonely-if
          // console.log('cancelling poll timer as status as tasks are complete');
          this.stopStatusPoll();
        }
      }
    }

    if (this.props.authd) {
      this.props.getMtzIfNeeded();
    }
  }

  componentWillUnmount() {
    if (this.timerId) {
      // console.log('cancelling poll timer as component will unmount');
      clearInterval(this.timerId);
    }
  }

  handleClickMtz() {
    if (!this.props.mtz) {
      this.props.materialize();
    } else {
      this.props.dematerialize();
    }
    this.setState({
      inTransition: true,
    });
  }

  startStatusPoll() {
    if (this.timerId === null) {
      // console.log('starting status poll');
      this.timerId = setInterval(
        () => {
          this.pollForStatus();
        },
        2000, // poll every two seconds.
      );
    }
  }

  stopStatusPoll() {
    if (this.timerId) {
      clearInterval(this.timerId);
      this.timerId = null;

      this.setState({
        inTransition: false,
      });
    }
  }

  pollForStatus() {
    const { mtz, getStatus } = this.props;

    if (mtz) {
      // console.log('polling for status', mtz, status);
      getStatus();
    }
  }

  statusTables(tasks) {
    const { inTransition } = this.state;

    return Object.keys(tasks).map((site) => (
      <Accordion key={site} defaultActiveKey={site}>
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey={site}>
            {`Site status for ${site}`}
            <br />
            <small>{!inTransition ? 'Active' : 'Polling every 2 seconds'}</small>
          </Accordion.Toggle>
          <Accordion.Collapse eventKey={site}>
            <Card.Body>
              <Table striped hover>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Complete</th>
                    <th>Error</th>
                  </tr>
                </thead>
                <tbody>
                  { !Object.hasOwnProperty.call(tasks[site], 'tasks')
                    ? <tr key={`${site}.1`}><td>Materialzation status not found.</td></tr>
                    : tasks[site].tasks.map((task, i) => (
                      <tr key={`${task.name}.${i}`}>
                        <td>
                          {task.name}
                        </td>
                        <td>
                          {task.kind}
                        </td>
                        <td>
                          {task.complete === true
                            ? <span style={{ color: 'green' }}> True </span>
                            : <span style={{ color: 'red' }}> False </span>}
                        </td>
                        <td>
                          {task.error === null
                            ? <span style={{ color: 'green' }}> - </span>
                            : (
                              <span style={{ color: 'red' }}>
                                {' '}
                                {task.error}
                                {' '}
                              </span>
                            )}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    ));
  }

  render() {
    const {
      eid, rid, mtz, authd, status,
    } = this.props;

    const {
      inTransition,
    } = this.state;

    if (!authd) {
      return <Redirect to="/landing" />;
    }

    let tables = null;
    if (status && status.status && status.status.siteTasks) {
      tables = this.statusTables(status.status.siteTasks);
    }

    const hdrText = `Materialization for ${eid}/${rid}`;
    const btnText = mtz ? 'Dematerialize' : 'Materialize';

    return (
      <Accordion defaultActiveKey="0">
        <Card>
          <Accordion.Toggle as={Card.Header} eventKey="0">
            {hdrText}
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0">
            <Card.Body>
              <Button
                variant="outline-info"
                onClick={this.handleClickMtz}
                disabled={inTransition}
              >
                {btnText}
              </Button>
              { this.state.inTransition && !mtz && <Loading /> }
              { tables }
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    );
  }
}

const mapStateToProps = (state, props) => {
  const { pid, eid, rid } = props;
  return {
    authd: selectors.getIsAuthenticated(state),
    mtz: selectors.getMaterialization(state, pid, eid, rid),
    status: selectors.getMaterializationStatus(state, pid, eid, rid),
  };
};

const mapDispatchToProps = (dispatch, props) => {
  const { pid, eid, rid } = props;
  return {
    getMtzIfNeeded: () => dispatch(
      TbapiActions.fetchMaterializationIfNeeded(pid, eid, rid),
    ),
    getMtz: () => dispatch(
      TbapiActions.fetchMaterialization(pid, eid, rid),
    ),
    materialize: () => dispatch(
      TbapiActions.createMaterialization(pid, eid, rid),
    ),
    dematerialize: () => dispatch(
      TbapiActions.dematerialize(pid, eid, rid),
    ),
    getStatus: () => dispatch(
      TbapiActions.fetchMaterializationStatus(pid, eid, rid),
    ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MaterializationView);
