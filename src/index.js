/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import Tbui from 'components/Tbui/Tbui';

import configureStore from './lib/configureStore';

// Service worker
import registerServiceWorker from './registerServiceWorker';

import './assets/css/bootstrap.min.css';
import './assets/css/pe-icon-7-stroke.css';
import './assets/css/mergetb.css';

const history = createBrowserHistory();
const store = configureStore({ history });

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Route path="/" component={Tbui} key={0} />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('react-root'),
);

registerServiceWorker();
