import { push } from 'connected-react-router';
import Auth from 'lib/auth';

const LOGIN_REQUEST = 'LOGIN_REQUEST';
const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
const LOGIN_FAILURE = 'LOGIN_FAILURE';
const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
// const PROFILE_REQUEST = 'PROFILE_REQUEST';
const PROFILE_RECEIVE = 'PROFILE_RECEIVE';
const TOKEN_EXPIRED = 'TOKEN_EXPIRED';

const requestLogin = () => ({
  type: LOGIN_REQUEST,
});

const loginSuccess = () => ({
  type: LOGIN_SUCCESS,
});

const loginError = (message = '') => ({
  type: LOGIN_FAILURE,
  message,
});

const receiveLogout = () => ({
  type: LOGOUT_SUCCESS,
});

const receiveProfile = (profile) => ({
  type: PROFILE_RECEIVE,
  profile,
});

const tokenExpired = () => ({
  type: TOKEN_EXPIRED,
});

/**
 * Async action creators with redux-thunk
 */

function loginAsync() {
  return (dispatch) => {
    dispatch(requestLogin());
    Auth.login();
  };
}
// This is equiv to loginAsync above, but with different syntax.
function logoutAsync() {
  return (dispatch) => {
    Auth.logout();
    dispatch(receiveLogout());
  };
}

function getProfileAsync() {
  return (dispatch) => {
    Auth.getProfile((err, profile) => {
      dispatch(receiveProfile(profile));
    });
  };
}

function handleAuthCallbackAsync() {
  return (dispatch) => {
    Auth.handleAuthentication((err, data) => {
      if (err) {
        return dispatch(loginError(`${err.error}: ${err.errorDescription}`));
      }
      dispatch(loginSuccess(data));
      dispatch(push('/'));
      return dispatch(getProfileAsync());
    });
  };
}

export {
  loginError,
  loginSuccess,
  getProfileAsync as getProfile,
  handleAuthCallbackAsync as handleAuthCallback,
  loginAsync as login,
  logoutAsync as logout,
  tokenExpired,
};
