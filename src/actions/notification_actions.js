export const SET_NOTIFICATION_ACTION = 'SET_NOTIFICATION_ACTION';
export const CLEAR_NOTIFICATION_ACTION = 'CLEAR_NOTIFICATION_ACTION';
export const CLEAR_ALL_NOTIFICATIONS_ACTION = 'CLEAR_ALL_NOTIFICATIONS_ACTION';

let notifId = 0;

const Level = {
  DEBUG: 'DEBUG',
  INFO: 'INFO',
  WARNING: 'WARNING',
  ERROR: 'ERROR',
  CRITICAL: 'CRITICAL',
};

const SetNotifcation = (level, message) => ({
  type: SET_NOTIFICATION_ACTION,
  payload: {
    id: notifId++, // eslint-disable-line no-plusplus
    level,
    message,
    timestamp: Date.now(),
    view: true,
  },
});

const ClearNotification = (item) => ({
  type: CLEAR_NOTIFICATION_ACTION,
  item,
});

const ClearAllNotifications = () => ({
  type: CLEAR_ALL_NOTIFICATIONS_ACTION,
});

const Debug = (message) => SetNotifcation(Level.DEBUG, message);
const Info = (message) => SetNotifcation(Level.INFO, message);
const Warn = (message) => SetNotifcation(Level.WARN, message);
const Error = (message) => SetNotifcation(Level.ERROR, message);
const Critical = (message) => SetNotifcation(Level.CRITICAL, message);

export {
  Level,
  SetNotifcation,
  ClearNotification,
  ClearAllNotifications,

  Debug,
  Info,
  Warn,
  Warn as Warning,
  Error,
  Critical,
};
