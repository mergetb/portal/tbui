/* eslint-disable react/destructuring-assignment, react/jsx-props-no-spreading */
import React from 'react';
import {
  Card, Container, Row, Col,
} from 'react-bootstrap';
import Realization from 'components/Realization/Realization';

const RealizationView = (props) => {
  const { projectname: pid, experimentname: eid, realizationname: rid } = props.match.params;
  const title = `Realization for ${pid}/${eid}/${rid}`;
  return (
    <Card>
      <Card.Header>
        {' '}
        {title}
        {' '}
      </Card.Header>
      <Card.Body>
        <Container fluid>
          <Row>
            <Col md={12}>
              <Realization pid={pid} eid={eid} rid={rid} />
            </Col>
          </Row>
        </Container>
      </Card.Body>
    </Card>
  );
};

export default RealizationView;
