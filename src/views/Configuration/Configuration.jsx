import React from 'react';
import {
  Card, Container, Row, Col,
} from 'react-bootstrap';
import TBAPIEndpointConfiguration from 'components/Configuration/TBAPIEndpointConfiguration';

const Configuration = () => (
  <Card>
    <Card.Header>
      Configuration/Settings
    </Card.Header>
    <Card.Body>
      <Container fluid>
        <Row>
          <Col md={4}>
            <TBAPIEndpointConfiguration />
          </Col>
        </Row>
      </Container>
    </Card.Body>
  </Card>
);

export default Configuration;
